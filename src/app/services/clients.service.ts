import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  private clients:Client[] = [
    {
      name: 'Tabea',
      gender: 0,
      img: 'assets/img/temporal.png',
      birthdate: '1996-02-02',
      active: true,
    },
    {
      name: 'Javier',
      gender: 1,
      img: 'assets/img/temporal.png',
      birthdate: '1994-06-02',
      active: true,
    },
    {
      name: 'Sini',
      gender: 0,
      img: 'assets/img/temporal.png',
      birthdate: '1996-11-02',
      active: true,
    }
  ];

  constructor() {
  }

  getClients():Client[] {
    return this.clients;
  }

  getClient(id:string) {
    return this.clients[id];
  }

  searchClients(word:string) {
    let clientsArray:Client[] = [];
    word = word.toLowerCase();

    for(let client of this.clients) {
      let name = client.name.toLowerCase();
      if(name.indexOf(word) >= 0 ) {
        clientsArray.push(client)
      }
    }
    return clientsArray;
  }
}

export interface Client {
  name: string,
  gender: Number,
  img: string,
  birthdate: string,
  active: Boolean
};