import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientsService } from '../../services/clients.service';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {

  clients:any[] = []
  word:string;

  constructor(
    private activatedRoute:ActivatedRoute,
    private _clientsService:ClientsService,
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.clients = this._clientsService.searchClients(params['word']);
      this.word = params['word'];
    });
  }

}
