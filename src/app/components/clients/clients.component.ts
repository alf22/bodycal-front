import { Component, OnInit } from '@angular/core';
import { ClientsService, Client } from '../../services/clients.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients:Client[] = [];

  constructor(
    private _clientsService:ClientsService,
    private _router:Router,
  ) {

  }

  ngOnInit(): void {
    this.clients = this._clientsService.getClients();
    console.log(this.clients);
  }

  seeClient(id:number) {
    this._router.navigate(['/client', id])
  }
}
