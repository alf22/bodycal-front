import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientsService } from '../../services/clients.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  client:any = {};

  constructor(
    private route: ActivatedRoute,
    private _clientsService:ClientsService
    ) {
      this.route.params.subscribe(params => {
        this.client = this._clientsService.getClient(params['id']);
      });
  }

  ngOnInit(): void {
  }

}
